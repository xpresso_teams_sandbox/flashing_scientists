"""
This is the implementation of data preparation for sklearn
"""

import sys
import logging
import pandas as pd
import os
import re
from statsmodels.tsa.arima_model import ARIMA
import datetime

from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger

__author__ = "### Author ###"

logger = XprLogger("data_train",level=logging.INFO)


class DataTrain(AbstractPipelineComponent):
    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self,train_file):
        super().__init__(name="DataTrain")
        """ Initialize all the required constansts and data her """
        self.train_data = pd.DataFrame()
        if not os.path.exists(self.OUTPUT_DIR):
            os.makedirs(self.OUTPUT_DIR)
        try:
            self.train_data = pd.read_csv(os.path.join('/data', train_file))
            print(self.train_data.head())
        except(Exception):
            print('no train file found')
            logger.error(Exception)

    def start(self, run_name):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            run_name: xpresso run name which is used by base class to identify
               the current run. It must be passed. While running as pipeline,
               Xpresso automatically adds it.

        """
        super().start(xpresso_run_name=run_name)
        # === Your start code base goes here ===
        logger.info("Saving config file for models")
        self.train_data.sort_values(by=['Date'], inplace=True, ascending=True)
        self.train_data.set_index('Date', inplace=True)
        self.train_data.head()
        self.train_data.index = pd.to_datetime(self.train_data.index)
        # self.train_data.index.freq = 'D'
        columns = self.train_data.columns
        countries = pd.DataFrame(self.train_data.columns, columns=['Country'])
        print(columns)
        countries.to_csv(os.path.join(self.OUTPUT_DIR,'countries.csv'), index=False)
        start_date = len(self.train_data)
        date = open(os.path.join(self.OUTPUT_DIR, 'date.txt'), "w")
        date.write(str(start_date))
        date.close()
        date = self.train_data.index.min()
        self.train_data['temp'] = 0
        s = self.train_data.shape[0]
        for column in columns:
            date_1, date_2 = date, date
            for i in range(1, s):
                date_1 = date_2
                date_2 = date_1 + datetime.timedelta(days=1)
                if i == s:
                    break
                else:
                    self.train_data.loc[date_2, 'temp'] = abs(self.train_data.loc[date_2, column] - self.train_data.loc[date_1, column])
            self.train_data[column] = self.train_data['temp']
        logging.info("Training pipeline begins...")
        try:
            for column in columns:
                model = ARIMA(self.train_data[column], order=(1, 0, 0))
                results = model.fit(disp=0)
                if column != 'Taiwan*' :
                    results.save(open(os.path.join(self.OUTPUT_DIR, '{}.pkl'.format(column)), 'wb'))
                    print(column)
                else:
                    column = re.sub('[^A-Za-z0-9_]+', '', column)
                    print(str(column))
                    results.save(open(os.path.join(self.OUTPUT_DIR, 'Taiwan.pkl'), 'wb'))
                    print(column)
        except():
            logger.info('Training data might be empty or problem with column name')
            print('model_not_saved')
        logging.info("Saved model... load model")
        self.completed(True)

    def send_metrics(self):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        report_status = {
            "status": {"status": "data_preparation"},
            "metric": {"metric_key": 1}
        }
        self.report_status(status=report_status)

    def completed(self, push_exp=False):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned

        """
        # === Your start code base goes here ===
        super().completed(push_exp=push_exp)

    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        super().terminate()

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        super().pause()

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        super().restart()


if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/app.py

    data_prep = DataTrain(
        train_file = 'combined_train.csv'
    )
    if len(sys.argv) >= 2:
        data_prep.start(run_name=sys.argv[1])
    else:
        data_prep.start(run_name="")
