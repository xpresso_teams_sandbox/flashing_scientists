"""
This is a sample hello world flask app
It only has a root resource which sends back hello World html text
"""

import logging
import pandas as pd
import os
import re
from statsmodels.tsa.arima_model import ARIMAResults
from xpresso.ai.core.data.inference import AbstractInferenceService
from xpresso.ai.core.logging.xpr_log import XprLogger

__author__ = "### Author ###"

logger = XprLogger("inference_service",level=logging.INFO)


class InferenceService(AbstractInferenceService):
    """ Main class for the inference service. User will need to implement
    following functions:
       - load_model: It gets the directory of the model stored as parameters,
           user will need to implement the method for loading the model and
           updating self.model variable
       - transform_input: It gets the JSON object from the rest API as input.
           User will need to implement the method to convert the json data to
           relevant feature vector to be used for prediction
       - predict: It gets the feature vector or any other object from the
           transform_input method. User will need to implement the model
           prediction codebase here. It returns the predicted value
       - transform_output: It gets the predicted value from the predict method.
           User need to implement this method to converted predicted method
           to JSON serializable object. Response from this method is send back
           to the client as JSON Object.

    AbstractInferenceService automatically creates the flask reset api
    with resource /predict for this class.
    Request JSON format:
       {
         "input" : <input_goes_here> // It could be any JSON object
       }
       This value of "input" key is sent to transform_input

    Response JSON format:
       {
         "message": "success/failure",
         "results": <response goes here> // It could be any JSOn object
       }
       Output of transform_output goes as value of "results"
    """

    def __init__(self):
        super().__init__()
        """ Initialize any static data required during boot up """
        self.config_file = 'countries.csv'
        self.INPUT_DIR = None
        self.date, self.start, self.end = None, None, None
        self.countries = pd.DataFrame()
        self.models = {}
        self.to_predict ={}
        self.all_cols = ['Country']

    def load_model(self, model_path):
        """ This is used to load the model on the boot time.
        User will need to load the model and save it in the variable
        self.model. It is IMPORTANT to update self.model.
        Args:
            model_path(str): Path of the directory where the model files are
               stored.
        """
        self.INPUT_DIR = model_path
        self.countries = pd.read_csv(os.path.join(self.INPUT_DIR, self.config_file))
        self.date = open(os.path.join(self.INPUT_DIR, 'date.txt'), "r+")
        self.start = int(self.date.read())
        self.end = self.start + 13
        countries = list(self.countries.Country)
        for country in countries:
            if country not in self.models:
                if country != 'Taiwan*' :
                    column = re.sub('[^A-Za-z0-9_]+', '', country)
                    print(str(country))
                    self.models[country] = ARIMAResults.load(open(os.path.join(self.INPUT_DIR, "{}.pkl".format(country)), 'rb'))
                else:
                    column = re.sub('[^A-Za-z0-9_]+', '', country)
                    print(str(country))
                    self.models[country] = ARIMAResults.load(open(os.path.join(self.INPUT_DIR, "Taiwan.pkl"), 'rb'))
        print(len(self.models.keys()))
        print(self.models)
        pass

    def transform_input(self, input_request):
        """
        Convert the raw input request to a feature data or any other object
        to be used during prediction

        Args:
            input_request: JSON Object or an array received from the REST API,
               which needs to be converted into relevant feature data.

        Returns:
            obj: Any transformed object
        """
        return input_request

    def predict(self, input_request):
        """
        This method implements the prediction method of the supported model.
        It gets the output of transform_input as input and returns the predicted
        value

        Args:
            input_request: Transformed object outputted from transform_input
               method

        Returns:
            obj: predicted value from the model

        """
        test_data =  pd.read_excel(os.path.join('/inf_data', input_request["test_data_file"]))
        pred_path = os.path.join('/inf_data', input_request['predictions_location'])

        # test_data = pd.read_excel(input_request["test_data_file"])
        # pred_path = input_request['predictions_location']
        country = list(test_data.Country)
        for key in country:
            if key in self.models:
                self.to_predict[key] = self.models[key].predict(start=self.start, end=self.end)
            else:
                print("model not found")
            logging.info(self.to_predict)
            logging.info(type(self.to_predict))
            # print(self.to_predict)
        prediction = pd.DataFrame.from_dict(self.to_predict)
        prediction = prediction.reset_index()
        prediction = pd.melt(prediction, id_vars=['index'], value_vars=country, var_name='Country',
                         value_name='Confirmed')
        prediction.rename(columns={"index": "Date"}, inplace=True)
        prediction['Confirmed'] =  prediction['Confirmed'].abs()
        if not os.path.exists(pred_path):
            os.makedirs(pred_path)
        prediction.to_csv(os.path.join(pred_path, "predictions.csv"), index=False)
        print(prediction.head())

        pass


    def transform_output(self, output_response):
        """
        Convert the predicted value into a relevant JSON serializable object.
        This is sent back to the REST API call
        Args:
            output_response: Predicted output from predict method.

        Returns:
          obj: JSON Serializable object
        """
        pass


if __name__ == "__main__":
    pred = InferenceService()
    # === To run locally. Use load_model instead of load. ===
    # pred.load_model(model_path="/output") #instead of pred.load()
    pred.load()
    pred.run_api(port=5000)
